\section{Stiefel manifolds}
\begin{construction}
  Let $0\leq k \leq n$.
  The \emph{Stiefel manifold} is
  \[
  \stf_{k,n}
  \defined
  \lset
  (v_1,\ldots,v_k)
  \in
  \left(\rr^n\right)^k
  \ssp
  \genby{v_i,v_j}
  =
  \begin{cases}
    1&
    \text{if }i=j
    \\
    0
    &
    \text{if }i\neq j.
  \end{cases}
  \rset
  \]
  We endow $\skn$ with the subspace topology of $\left(\rr^n\right)^k$;
  since $\skn\sse \left(\sphere^{n-1}\right)^k$ is closed, it is compact.
\end{construction}
\begin{example}
  \leavevmode
  \begin{enumerate}
    \item $\stf_{0,n} = \lset \ast\rset$ is the one-point space for every $n\geq 1$.
    \item $\stf_{1,n} = \sphere^{n-1}$.
    \item Denote by $\oo(n)$ the orthogonal group.
    Then the map
    \begin{align*}
      \stf_{n,n}
      &
      \to
      \oo(n)
      \\
      \left(v_1,\ldots,v_n\right)
      &
      \mapsto
      \left(
      \begin{array}{c|c|c}
        v_1
        &
        \ldots
        &
        v_n
      \end{array}
      \right)
    \end{align*}
    is an isomorphism.
    \item Denote by $\so(n)$ the special orthogonal group.
    Then the map
    \begin{align*}
      \so(n)
      &
      \to
      \stf_{n-1,n}
      \\
      A
      &
      \mapsto
      \left(Ae_1,\ldots,A_{n-1}\right)
    \end{align*}
    is a continous bijection between compact Hausdorff spaces, hence a homeomorphism.
  \end{enumerate}
\end{example}
\begin{prop}
  The space $\skn$ is a manifold of dimension
  \[
  (n-1)+(n-2)+\ldots +(n-k)
  =
  nk
  -
  \frac{k(k+1)}{2}.
  \]
\end{prop}
\begin{proof}
  We do this by induction on $k$.
  In the case $k=0$, we have $\stf_{0,n} = \lset \ast\rset$, which is a zero-dimensional manifold.
  Similarly, in the case $k=1$, we have $\stf_{1,n} = \sphere^{n-1}$, which is a $(n-1)$-dimensional manifold.
  Now suppose $k\geq 2$.
  We write $\sphere^{n-1}_{+}$ for the ``northern hemisphere'' of $\sphere^{n-1}$:
  \[
  \sphere^{n-1}_{+}\defined
  \lset
  w\in \sphere^{n-1}\ssp w_1>0
  \rset,
  \]
  and use Gram-Schmidt to define a map
  \[\psi\mc \sphere^{n-1}_{+}\to \oo(n)\]
  as the composition
  \[
  \begin{tikzcd}[cramped]
    \sphere^{n-1}_{+}
    \ar{r}
    &
    \gl_n(\rr)
    \ar{r}[above]{\mathrm{GS}}
    &
    \oo(n)
  \end{tikzcd},
  \]
  where the map
  \begin{align*}
  \sphere^{n-1}_{+}
  &\to
  \gl_n(\rr)
  \intertext{is given by}
  w
  &
  \mapsto
  \left(
  \begin{array}{c|c|c|c|c}
  w
  &
  e_2
  &
  e_3
  &
  \ldots
  &
  e_n
  \end{array}
  \right).
  \end{align*}
Then $\psi$ is continous, $\psi(e_1) = E_n$ and $\psi(w)\cdot e_1  = w$ holds for all $w\in \sphere^{n-1}_{+}$.
Note that it is not possible to extend this contruction to all of $\sphere^{n-1}$, in the sense that there is no continous map
\[
\psi'\mc \sphere^{n-1}\to \oo(n)
\]
such that $\psi'(w)\cdot e_1 = w$ holds for all $w\in \sphere^{n-1}$.
Define now
\[
  U
  \defined
  \lset
  (v_1,\ldots,v_k)\in \skn
  \ssp
  v_1 \in \sphere^{n-1}_{+}\rset .
  \]
  \begin{claim}
    The map
    \begin{align*}
      U
      &
      \to
      \sphere^{n-1}_{+}\times \stf_{k-1,n-1}
      \\
      (v_1,\cdot,v_k)
      &
      \mapsto
      \left(
      v_1,\psi^{-1}(v_1)v_2,\ldots,\psi^{-1}(v_1)v_k
      \right)
    \end{align*}
    is a well-defined homeomorphism.
  \end{claim}
  From this, we conclude that the point $(e_1,\ldots,e_n)\in \skn$ has an open neighborhood which is homeomorphic to $\sphere^{n-1}_{+}\times \stf_{k-1,n-1}$.
  By induction, the latter is a manifold of dimension
  \[
  d\defined (n-1)+(n-2)+\ldots + \left((n-1)-(k-1)\right),
  \]
  and in particular, $(e_1,\ldots,e_n)$ has an open neighborhood homeomorphic to $\rr^d$.
  \par
  Let now $(v_1,\ldots,v_n)\in \skn$ be any point, and complete it to an orthonormal basis, which gives a matrix
  \[
  A\defined
  \left(
  \begin{array}{c|c|c|c|c|c}
    v_1
    &
    \ldots
    &
    v_{k}
    &
    v_{k}
    &
    \ldots
    &
    v_n
  \end{array}
  \right)\in \oo(n).
  \]
  Then
  \[
  A\mc \skn \to \skn,~
  (w_1,\ldots,w_k)
  \mapsto
  (Aw_1,\ldots Aw_k)
  \]
  is a self-homeomorphism of $\skn$, which sends $(e_1,\ldots,e_n)$ to $(v_1,\ldots,v_k)$, and so $(v_1,\ldots,v_k)$ has an open neighborhood which is homeomorphic to $\rr^d$ too.
\end{proof}
