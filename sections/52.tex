\section{The Eilenberg-Zilber theorem}
\begin{defn}
  A \emph{simplicial abelian group} is a functor
  \[
  \sgroup{A}\mc \IDelta\op \to \abcat
  \]
  We denote the category of simplicial abelian groups by $\sabcat$.
\end{defn}
\begin{example}
  Let $X$ be a simplicial set and $A$ an abelian group.
  Then the free abelian group functor $A[-]\mc \setcat\to \abcat$ turns $X$ into a simplicial abelian group:
  \[
  \begin{tikzcd}
    \IDelta\op
    \ar{r}[above]{X}
    &
    \setcat
    \ar{r}[above]{A[-]}
    &
    \abcat
  \end{tikzcd}
  \]
\end{example}
\begin{construction}
  Let $\sgroup{A}$ be a simplicial abelian group.
  The \emph{chain complex} $\linearization(A)$ is the chain complex with:
  \[
  \linc(A)
  =
  \begin{cases}
    A_n
    &
    \text{if }n\geq 0;
    \\
    0
    &
    \text{otherwise.}
  \end{cases}
  \]
  and differentials:
  \[
  d_n\mc A_n\to A_{n-1},
  ~
  x
  \mapsto
  \sum_{i=0}^n
  \left(-1\right)^n d_i^{\ast}(x).
  \]
\end{construction}
\begin{rem}
  We have the following factorization:
  \[
  \begin{tikzcd}
  \sset
  \ar{rr}[above]{\linearization(-;A)}
  \ar{rd}[below left]{A[-]}
  &
  &
  \chaincat_{\geq 0}(A)
  \\
  &
  \sabcat
  \ar{ur}[below right]{\linearization}
  &
  \end{tikzcd}
  \]
\end{rem}
\begin{construction}
  Let $\sgroup{A}$ and $\sgroup{B}$ be two simplicial abelian groups.
  Then their \emph{tensor product} $\sgroup{A}\tensor\sgroup{B}$ is defined as the composition
  \[
  \begin{tikzcd}
    \IDelta\op
    \ar{r}[above]{\mathrm{diag}}
    &
    \IDelta\op \times \IDelta\op
    \ar{r}[above]{\sgroup{A}\times\sgroup{B}}
    &
    \abcat\times\abcat
    \ar{r}[above]{\tensor}
    &
    \abcat
  \end{tikzcd}
  \]
\end{construction}
\begin{construction}
\leavevmode
\begin{enumerate}
  \item
  Let $\sgroup{A},\sgroup{B}$ be two simplical abelian groups.
  The \emph{Alexander-Whitney map} is a map
  \begin{align*}
  \aw\mc \linearization(A\tensor B)
  &
  \to
  \linearization(A)
  \tensor
  \linearization(B),
  \intertext{which is defined in degree $n$ as }
  a\tensor b
  &
  \mapsto
  \sum_{p+q=n}
  d_{\mathrm{front}}^{\ast}(a)
  \tensor
  d_{\mathrm{back}}^{\ast}(b)
  \end{align*}
  \item
  Let $p,q\geq 0$ be integers.
  A \emph{$(p,q)$-shuffle} is a permutation
  \[
  \sigma\mc
  \lset 0,1,\ldots,p+q-1\rset
  \isomorphism
  \lset 0,1,\ldots,p+q-1\rset,
  \]
  such that both
  \[
  \restrict{\sigma}{\lset 0,\ldots,p-1\rset}
  \text{ and }
  \restrict{\sigma}{\lset p,\ldots,p+q-1\rset}
  \]
  are monotonous.
  \item
  Let again $\sgroup{A},\sgroup{B}$ be two simplicial abelian groups.
  Then the \emph{Eilenberg-Zilber map} or \emph{shuffle product} is a map
  \[
  \nabla\mc
  \linearization(A)
  \tensor
  \linearization(B)
  \to
  \linearization(A\tensor B),
  \]
  which is defined in degrees $p$ and $q$ as
  \[
  a\tensor b
  \overset{\nabla_{p,q}}{\longmapsto}
  \sum_{\substack{\sigma:\\(p,q)\text{-shuffle}}}
  (-1)^{\sigma}
  \cdot
  \left(s_{\nu_1}\circ \ldots \circ s_{\nu_q}\right)^{\ast}(a)
  \tensor
  \left(s_{\mu_1}\circ \ldots \circ s_{\mu_p}\right)^{\ast}(b).
  \]
\end{enumerate}
\end{construction}
\begin{lem}
  The shuffle products for varying $(p,q)$ define a chain map, i.e. it holds that
  \[
  d_{p+q}(\nabla_{p,q}(a\tensor b))
  =
  \nabla_{p-1,q}(d_p(a)\tensor b)
  +
  (-1)^p
  \nabla_{p,q-1}(a\tensor d_q(b)).
  \]
\end{lem}
\begin{lem}
  Let $\sgroup{A},\sgroup{B}$ be simplical abelian groups.
  Then the collection of Alexander-Whitney maps $\aw_n$ define a chain map
  \[
  \linearization(A\tensor B)
  \to
  \linearization(A)\tensor \linearization(B).
  \]
\end{lem}
